package com.pass.tdr.dataextractor;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DataextractorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataextractorApplication.class, args);


    }

    @Bean
    ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
