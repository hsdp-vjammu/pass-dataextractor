package com.pass.tdr.dataextractor.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties
@Data
public class YAMLConfig {


    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.data-username}")
    private String username;

    @Value("${spring.datasource.data-password}")
    private String password;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;


    @Value("${spring.jpa.database-platform}")
    private String databasePlatform;

    @Value("${spring.jpa.show-sql}")
    private Boolean showSQL;

    @Value("${spring.jpa.hibernate.naming.physical-strategy}")
    private String hibernateNamingStrategy;

    @Value("${spring.jpa.hibernate.jdbc.batch_size}")
    private String JDBCBatchSize;

    @Value("${hsdp-tdrdata.tdr-token-endpoint}")
    private String TDR_Token_Endpoint;


}
