package com.pass.tdr.dataextractor.controller;

import com.pass.tdr.dataextractor.dto.Feedback;
import com.pass.tdr.dataextractor.dto.FeedbackQuery;
import com.pass.tdr.dataextractor.dto.TimeSeries;
import com.pass.tdr.dataextractor.entity.PassTimeSeries;
import com.pass.tdr.dataextractor.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class QuestionController {

    private List<PassTimeSeries> passTimeSeries;


    @Autowired
    List<TimeSeries> timeSeriesList;

    @Autowired
    private QuestionService questionService;
    @Autowired
    private FeedbackQuery feedbackQuery;

    @RequestMapping(value = "/feedback", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Feedback>> queryDataItem(@RequestParam String serialNo,
                                                        @RequestParam String subjectUUID,
                                                        @RequestParam long feedbackTimeStampStart,
                                                        @RequestParam long feedbackTimeStampEnd,
                                                        @RequestParam(value = "page", defaultValue = "0") int page)


    {


        feedbackQuery.setSerialNo(serialNo);
        feedbackQuery.setSubjectUUID(subjectUUID);
        feedbackQuery.setFeedbackTimeStampStart(feedbackTimeStampStart);
        feedbackQuery.setFeedbackTimeStampEnd(feedbackTimeStampEnd);

        return questionService.fetchFeedback(page, feedbackQuery);


    }  // @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date requestTimeStampStart,
}
