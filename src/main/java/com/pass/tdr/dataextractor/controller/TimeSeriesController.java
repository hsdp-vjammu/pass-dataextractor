package com.pass.tdr.dataextractor.controller;

import com.pass.tdr.dataextractor.dto.TDRQuery;
import com.pass.tdr.dataextractor.dto.TimeSeries;
import com.pass.tdr.dataextractor.entity.PassTimeSeries;
import com.pass.tdr.dataextractor.service.TimeSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class TimeSeriesController {


    private List<PassTimeSeries> passTimeSeries;


    @Autowired
    List<TimeSeries> timeSeriesList;

    @Autowired
    private TimeSeriesService timeSeriesService;
    @Autowired
    private TDRQuery tdrQuery;

    @RequestMapping(value = "/tdr/dataitem/hello", method = RequestMethod.GET)
    public String hello() {
        return "hello there!!";
    }

    @RequestMapping(value = "/timeseries", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TimeSeries>> queryDataItem(@RequestParam(defaultValue = "SRCPASS") String organization,
                                                          @RequestParam(defaultValue = "PHbData") String dataTypeSystem,
                                                          @RequestParam(defaultValue = "PHbDataBlob") String dataTypeCode,
                                                          @RequestParam(defaultValue = "Phb") String deviceType,
                                                          @RequestParam long requestTimeStampStart,
                                                          @RequestParam long requestTimeStampEnd,
                                                          @RequestParam String serialNo,
                                                          @RequestParam String subjectUUID,
                                                          @RequestParam(value = "page", defaultValue = "0") int page)
    //@RequestParam(value = "limit", defaultValue = "30") int limit)

    {
        tdrQuery.setOrganization(organization);
        tdrQuery.setDevice(deviceType);
        tdrQuery.setSerialNo(serialNo);
        tdrQuery.setDataTypeSystem(dataTypeSystem);
        tdrQuery.setDataTypeCode(dataTypeCode);
        tdrQuery.setSubjectUUID(subjectUUID);
        tdrQuery.setRequestTimeStampStart(requestTimeStampStart);
        tdrQuery.setRequestTimeStampEnd(requestTimeStampEnd);

        return timeSeriesService.FetchTimeSeriesData(page, tdrQuery);


    }  // @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date requestTimeStampStart,
    //@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date requestTimeStampEnd,


}
