package com.pass.tdr.dataextractor.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class Feedback {
    //private Long timeStamp;

    private String subjectuuid;
    private String serialNo;
    private Long feedbackTimeStamp;
    private Long notificationFireTimeStamp;
    private String question;
    private String feedback;


}
