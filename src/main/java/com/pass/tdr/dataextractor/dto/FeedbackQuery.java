package com.pass.tdr.dataextractor.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class FeedbackQuery {


    public String serialNo;

    public String subjectUUID;
    public long feedbackTimeStampStart;
    public long feedbackTimeStampEnd;


}
