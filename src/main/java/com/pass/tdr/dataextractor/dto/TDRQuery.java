package com.pass.tdr.dataextractor.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class TDRQuery {

    public String organization;
    public String device;
    public String serialNo;
    public String dataTypeSystem;
    public String dataTypeCode;
    public String subjectUUID;
    public long requestTimeStampStart;
    public long requestTimeStampEnd;


}
