package com.pass.tdr.dataextractor.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
public class TimeSeries {

    private Long timeStamp;
    private Integer heartRate;
    private Integer restingHeartRate;
    private Integer recoveryHeartRate;
    private Integer respirationRate;
    private Integer steps;
    private double activeEnergyExpenditure;
    private double totalEnergyExpenditure;
    private Integer activeMinutes;
    private Integer activityCounts;
    private Integer batteryLevel;
    private Integer cardioFitnessIndex;
    private Integer vo2Max;
    private Integer offWrist;
    private Integer run;
    private Integer walk;
    private Integer cycle;
}
