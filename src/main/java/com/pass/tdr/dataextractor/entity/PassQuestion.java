package com.pass.tdr.dataextractor.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "passquestions", schema = "passdata")
public class PassQuestion {

    @Id
    @Column
    @SequenceGenerator(name = "mySeqGenQuestion", sequenceName = "passdata.passquestions_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "mySeqGenQuestion")
    private Long id;

/*    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "TDR_Status_Id", nullable = false)
    @JsonFormat
    private TDRStatus tdrStatus;*/


    @Column(name = "Timestamp", nullable = false)
    private Long timesStamp;


    @Column(name = "SubjectUUID", nullable = false)
    private String subjectUUID;

    @Column(name = "SerialNo", nullable = false)
    private String serialNo;

    @Column(name = "Question")
    private String question;

    @Column(name = "notificationFireTimeStamp")
    private Long notificationFireTimeStamp;

    @Column(name = "Feedback")
    private String feedback;

    @Column(name = "FeedbackTimestamp")
    private Long feedbackTimestamp;

    public PassQuestion(Long timesStamp) {
        this.timesStamp = timesStamp;
    }


    public PassQuestion() {
        this.timesStamp = 0L;
        this.subjectUUID = "";
        this.serialNo = "";
        this.question = "";
        this.notificationFireTimeStamp = 0L;
        this.feedback = "";
        this.feedbackTimestamp = 0L;

    }

    public PassQuestion(TDRStatus tdrStatus, String SubjectUUID) {
        //   this.tdrStatus = tdrStatus;
        this.timesStamp = 0L;
        this.subjectUUID = "";
        this.serialNo = "";
        this.question = "";
        this.notificationFireTimeStamp = 0L;
        this.feedback = "";
        this.feedbackTimestamp = 0L;
        this.subjectUUID = SubjectUUID;
        this.serialNo = tdrStatus.getSerialNo();
    }


}
