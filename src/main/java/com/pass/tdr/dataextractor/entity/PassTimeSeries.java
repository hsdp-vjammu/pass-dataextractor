package com.pass.tdr.dataextractor.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "passtimeseries", schema = "passdata")
public class PassTimeSeries implements Comparable<PassTimeSeries> {

    @Id
    @Column
    @SequenceGenerator(name = "mySeqGen", sequenceName = "passdata.passtimeseries_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "mySeqGen")
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "TDR_Status_Id", nullable = false)
    @JsonFormat
    private TDRStatus tdrStatus;

    @Column(name = "Timestamp", nullable = false)
    private Long timeStamp;

    @Column(name = "HeartRate", nullable = false)
    private Integer heartRate;

    @Column(name = "RestingHeartRate", nullable = false)
    private Integer restingHeartRate;

    @Column(name = "RecoveryHeartRate", nullable = false)
    private Integer recoveryHeartRate;

    @Column(name = "RespirationRate", nullable = false)
    private Integer respirationRate;

    @Column(name = "Steps", nullable = false)
    private Integer steps;

    @Column(name = "ActiveEE", nullable = false)
    private double activeEnergyExpenditure;

    @Column(name = "TotalEE", nullable = false)
    private double totalEnergyExpenditure;

    @Column(name = "ActiveMinutes", nullable = false)
    private Integer activeMinutes;

    @Column(name = "ActivityCounts", nullable = false)
    private Integer activityCounts;

    @Column(name = "BatteryLevel", nullable = false)
    private Integer batteryLevel;

    @Column(name = "CardioFitnessIndex", nullable = false)
    private Integer cardioFitnessIndex;

    @Column(name = "VO2Max", nullable = false)
    private Integer vo2Max;


    @Column(name = "OffWrist", nullable = false)
    private Integer offWrist;

    @Column(name = "Run", nullable = false)
    private Integer run;

    @Column(name = "Walk", nullable = false)
    private Integer walk;

    @Column(name = "Cycle", nullable = false)
    private Integer cycle;


    @Column(name = "SleepIntent", nullable = false)
    private Integer sleepIntent;

    @Column(name = "subjectuuid", nullable = false)
    private String subjectUUID;

    @Column(name = "serialNo", nullable = false)
    private String serialNo;



    public PassTimeSeries() {
        this.timeStamp = 0L;
        this.heartRate = 0;
        this.restingHeartRate = 0;
        this.recoveryHeartRate = 0;
        this.respirationRate = 0;
        this.steps = 0;
        this.activeEnergyExpenditure = 0;
        this.totalEnergyExpenditure = 0;
        this.activeMinutes = 0;
        this.activityCounts = 0;
        this.batteryLevel = 0;
        this.cardioFitnessIndex = 0;
        this.vo2Max = 0;
        this.offWrist = 0;
        this.walk = 0;
        this.run = 0;
        this.cycle = 0;
        this.sleepIntent = 0;
        this.subjectUUID = "";
        this.serialNo = "";
    }

 /* //  public PassTimeSeries(Long timesStamp) {
        this.timeStamp = timesStamp;
    }*/

    public PassTimeSeries(TDRStatus tdrStatus) {
        this.tdrStatus = tdrStatus;
        this.timeStamp = 0L;
        this.heartRate = 0;
        this.restingHeartRate = 0;
        this.recoveryHeartRate = 0;
        this.respirationRate = 0;
        this.steps = 0;
        this.activeEnergyExpenditure = 0;
        this.totalEnergyExpenditure = 0;
        this.activeMinutes = 0;
        this.activityCounts = 0;
        this.batteryLevel = 0;
        this.cardioFitnessIndex = 0;
        this.vo2Max = 0;
        this.offWrist = 0;
        this.walk = 0;
        this.run = 0;
        this.cycle = 0;
        this.sleepIntent = 0;
        this.subjectUUID = "";
        this.serialNo = "";
    }


    @Override
    public int compareTo(PassTimeSeries o) {
        long comparedates = ((PassTimeSeries) o).getTimeStamp();
        return (int) (this.getTimeStamp() - comparedates);

    }
}
