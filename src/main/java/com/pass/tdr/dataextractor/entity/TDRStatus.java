package com.pass.tdr.dataextractor.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "tdrstatus", schema = "passdata")
public class TDRStatus {

    @Id
    @SequenceGenerator(name = "mySeqGen", sequenceName = "passdata.tdrstatus_id_seq", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "mySeqGen")
    @Column
    private long id;


    @Column(name = "SubjectId", nullable = false)
    private String subjectId;


    @Column(name = "SerialNo", nullable = false)
    private String serialNo;


    @Column(name = "DeviceType", nullable = false)
    private String deviceType;


    @Column(name = "LocationURL", columnDefinition = "text", nullable = false)
    private String locationURL;


    @NotNull
    @Column(name = "Date_Of_Birth", nullable = false)
    private Date dateOfBirth;


    @Column(name = "Created_By", nullable = false)
    @CreatedBy
    private String createdBy;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Created_Date", nullable = false)
    private java.util.Date createdDate;

    @Column(name = "Modified_By", nullable = false)
    @LastModifiedBy
    private String modifiedBy;

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Modified_Date", nullable = false)
    private java.util.Date modifiedDate;

    @NotNull
    @Column(name = "processed", nullable = false)
    private Boolean processed;







}
