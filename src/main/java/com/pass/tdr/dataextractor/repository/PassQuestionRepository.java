package com.pass.tdr.dataextractor.repository;

import com.pass.tdr.dataextractor.entity.PassQuestion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PassQuestionRepository extends PagingAndSortingRepository<PassQuestion, Long> {


    Page<PassQuestion> findBySubjectUUIDAndSerialNoAndFeedbackTimestampBetween(
            String subjectUUID,
            String serialNo,
            Long feedbackStartTime,
            Long feedbackEndTime,
            Pageable pageable
    );


}
