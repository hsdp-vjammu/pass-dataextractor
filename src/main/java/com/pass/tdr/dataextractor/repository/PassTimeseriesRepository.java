package com.pass.tdr.dataextractor.repository;


import com.pass.tdr.dataextractor.entity.PassTimeSeries;
import com.pass.tdr.dataextractor.entity.TDRStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
//public interface PassTimeseriesRepository extends JpaRepository<PassTimeSeries, Long> {
public interface PassTimeseriesRepository extends PagingAndSortingRepository<PassTimeSeries, Long> {


    Page<PassTimeSeries> findBySubjectUUID(String subjectUUID, Pageable pageable);

    Page<PassTimeSeries> findByTdrStatusAndTimeStampBetween(
            TDRStatus tdrStatus,
            Long analysisStart,
            Long analysisEnd, Pageable pageable
    );


    Page<PassTimeSeries> findBySubjectUUIDAndSerialNo(
            List<TDRStatus> tdrStatus,
            Long analysisStart,
            Long analysisEnd, Pageable pageable
    );

    Page<PassTimeSeries> findBySubjectUUIDAndSerialNoAndTimeStampBetween(
            String subjectUUID,
            String serialNo,
            Long analysisStart,
            Long analysisEnd,
            Pageable pageable
    );


    @Query(value = "SELECT * from passdata.PassTimeSeries pts WHERE " +
            "pts.subjectuuid= :subjectUUID AND pts.serialNo= :serialNo AND pts.timeStamp  BETWEEN :analysisStartDate AND :analysisEndDate", nativeQuery = true)
    Page<PassTimeSeries> GetTimeSeriesDataWithUUIDAndSerialNoAndTimeStampDateRange(@Param("subjectUUID") String subjectUUID,
                                                                                   @Param("serialNo") String serialNo,
                                                                                   @Param("analysisStartDate") long analysisStartDate,
                                                                                   @Param("analysisEndDate") long analysisEndDate, Pageable pageable);



    @Query(value = "SELECT * from passdata.PassTimeSeries pts WHERE " +
            "pts.tdr_status_id in :tdrStatusId AND pts.timeStamp  BETWEEN :analysisStartDate AND :analysisEndDate", nativeQuery = true)
    Page<PassTimeSeries> GetTimeSeriesDataWithTdrStatusIDAndTimeStampDateRange(@Param("tdrStatusId") List<TDRStatus> tdrStatusId,
                                                                               @Param("analysisStartDate") long analysisStartDate,
                                                                               @Param("analysisEndDate") long analysisEndDate, Pageable pageable);



    List<PassTimeSeries> findByTimeStampBetween(
            Long analysisStart,
            Long analysisEnd
    );


   /* @Query("SELECT pts.timesStamp, pts.steps from PassTimeSeries pts WHERE pts.tdrStatus= :tdrStatusId AND pts.timesStamp  >= :analysisStartDate AND pts.timesStamp<=:analysisEndDate")
    List<PassTimeSeries> GetTimeSeriesData(@Param("tdrStatusId") TDRStatus tdrStatusId,
                                           @Param("analysisStartDate") Long analysisStartDate,
                                           @Param("analysisEndDate") Long analysisEndDate);*/

    // List<PassTimeSeries> findByTdrStatusId(Long Id);


    //  List<PassTimeSeries> findByTimesStamp(Long timeStamp);

}
