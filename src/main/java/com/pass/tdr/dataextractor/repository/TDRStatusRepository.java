package com.pass.tdr.dataextractor.repository;


import com.pass.tdr.dataextractor.entity.TDRStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TDRStatusRepository extends JpaRepository<TDRStatus, Long> {


    List<TDRStatus> findBySubjectIdAndAndSerialNo(String subjectId, String serialNo);


 /*   @Query(value = "SELECT * from passdata.tdrstatus ts WHERE ts.subjectId = :subjectId AND ts.serialno = :serialNo", nativeQuery = true)
    List<TDRStatus> GETTDRStatusID(@Param("subjectId") String subjectId,
                                   @Param("serialNo") String serialNo);
*/


//            @Query("UPDATE PassTimeSeries pts SET pts.offWrist =1 WHERE pts.timesStamp > :startTimeStamp AND pts.timesStamp <= :endTimeStamp AND pts.tdrStatus= :tdrStatusId")
//            int updateOffWrist(@Param("startTimeStamp") Long startTimeStamp,
//            @Param("endTimeStamp")Long endTimeStamp,
//            @Param("tdrStatusId")TDRStatus tdrStatusId);
}
