package com.pass.tdr.dataextractor.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pass.tdr.dataextractor.dto.Feedback;
import com.pass.tdr.dataextractor.dto.FeedbackQuery;
import com.pass.tdr.dataextractor.entity.PassQuestion;
import com.pass.tdr.dataextractor.repository.PassQuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class QuestionService {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private PassQuestionRepository passQuestionRepository;

    @Autowired
    private List<Feedback> feedbackList;


    private final static int LIMIT = 60;

    private Long feedbackStartDate = 0L;
    private Long feedbackEndDate = 0L;

    public ResponseEntity<List<Feedback>> fetchFeedback(int page, FeedbackQuery feedbackQuery) {

        Pageable pageableRequest = PageRequest.of(page, LIMIT, Sort.Direction.ASC, "FeedbackTimestamp");
        feedbackList.clear();
        feedbackStartDate = feedbackQuery.getFeedbackTimeStampStart();
        feedbackEndDate = feedbackQuery.getFeedbackTimeStampEnd();

        HttpHeaders responseHeaders = new HttpHeaders();

        Page<PassQuestion> passQuestionPage = passQuestionRepository.findBySubjectUUIDAndSerialNoAndFeedbackTimestampBetween(feedbackQuery.getSubjectUUID(),
                feedbackQuery.getSerialNo(),
                feedbackStartDate, feedbackEndDate, pageableRequest);
        List<PassQuestion> passQuestionList = passQuestionPage.getContent();
        responseHeaders.set("totalElements", String.valueOf(passQuestionPage.getTotalElements()));
        responseHeaders.set("totalPages", String.valueOf(passQuestionPage.getTotalPages()));
        try {

            String arrayToJson = mapper.writeValueAsString(passQuestionList);
            TypeReference<List<PassQuestion>> mapType = new TypeReference<List<PassQuestion>>() {
            };
            feedbackList = mapper.readValue(arrayToJson, mapType);


        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(feedbackList);
    }


}


