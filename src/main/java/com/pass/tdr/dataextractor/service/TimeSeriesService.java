package com.pass.tdr.dataextractor.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pass.tdr.dataextractor.dto.TDRQuery;
import com.pass.tdr.dataextractor.dto.TimeSeries;
import com.pass.tdr.dataextractor.entity.PassTimeSeries;
import com.pass.tdr.dataextractor.entity.TDRStatus;
import com.pass.tdr.dataextractor.repository.PassTimeseriesRepository;
import com.pass.tdr.dataextractor.repository.TDRStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

@Service
public class TimeSeriesService {
    @Autowired
    private ObjectMapper mapper;

    private final static int LIMIT = 60;
    @Autowired
    PassTimeseriesRepository passTimeseriesRepository;

    @Autowired
    TDRStatusRepository tdrStatusRepository;


    @Autowired
    List<TimeSeries> timeSeriesList;

    private List<PassTimeSeries> passTimeSeries;
    private List<TDRStatus> tdrStatus;
    private Long analysisStartDate = 0L;
    private Long analaysisEndDate = 0L;

    public ResponseEntity<List<TimeSeries>> FetchTimeSeriesData(int page, TDRQuery tdrQuery) {

        Pageable pageableRequest = PageRequest.of(page, LIMIT, Sort.Direction.ASC, "timeStamp");

        Calendar calendar = Calendar.getInstance();
        timeSeriesList.clear();

        analysisStartDate = tdrQuery.getRequestTimeStampStart();//.getTime() / 1000;

        analaysisEndDate = tdrQuery.getRequestTimeStampEnd();

        HttpHeaders responseHeaders = new HttpHeaders();


        Page<PassTimeSeries> passTimeSeriesPage = passTimeseriesRepository.findBySubjectUUIDAndSerialNoAndTimeStampBetween(tdrQuery.getSubjectUUID(),
                tdrQuery.getSerialNo(), analysisStartDate, analaysisEndDate, pageableRequest);
        List<PassTimeSeries> passTimeSeries = passTimeSeriesPage.getContent();
        responseHeaders.set("totalElements", String.valueOf(passTimeSeriesPage.getTotalElements()));
        responseHeaders.set("totalPages", String.valueOf(passTimeSeriesPage.getTotalPages()));
        try {

            String arrayToJson = mapper.writeValueAsString(passTimeSeries);
            TypeReference<List<TimeSeries>> mapType = new TypeReference<List<TimeSeries>>() {
            };
            timeSeriesList = mapper.readValue(arrayToJson, mapType);


        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(timeSeriesList);
    }
}




